import {Component, OnInit} from '@angular/core';
import {ConfigService} from "../../config.service";
import {ConfigPanelComponent} from "../../config-panel/config-panel.component";
import {PanelService} from "../panel/panel.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private configService: ConfigService,
    private panelService: PanelService
  ) {
  }

  public apiConfigured: boolean = false;

  ngOnInit(): void {
    this.configService.isConfigured().subscribe(apiUrl => {
      this.apiConfigured = !!apiUrl
    })
  }


  public openConfigPanel() {
    ConfigPanelComponent.open(this.panelService, {});
  }

}
