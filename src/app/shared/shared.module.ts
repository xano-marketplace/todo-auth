import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from "./header/header.component";
import {FormGeneratorComponent} from "./form-generator/form-generator.component";
import {SafeHtmlPipe} from './safe-html.pipe';
import {PanelHeaderComponent} from './panel/panel-header/panel-header.component';
import {PanelFooterComponent} from './panel/panel-footer/panel-footer.component';
import {PanelInfoComponent} from './panel/panel-info/panel-info.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [HeaderComponent, FormGeneratorComponent, SafeHtmlPipe, PanelHeaderComponent, PanelFooterComponent, PanelInfoComponent],
  exports: [
    HeaderComponent,
    SafeHtmlPipe,
    PanelHeaderComponent,
    PanelInfoComponent,
    FormGeneratorComponent,
    PanelFooterComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    ReactiveFormsModule
  ]
})
export class SharedModule {
}
