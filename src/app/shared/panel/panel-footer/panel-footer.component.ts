import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faSave, faSpinner, faTrashAlt} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-panel-footer',
  templateUrl: './panel-footer.component.html',
  styleUrls: ['./panel-footer.component.scss']
})
export class PanelFooterComponent implements OnInit {
  @Output() onSave = new EventEmitter;
  @Output() onDelete = new EventEmitter;
  @Output() onGoBack = new EventEmitter;

  @Input() disabled;
  @Input() deleting;
  @Input() saving;
  @Input() saveType = "button";
  @Input() saveIcon = faSave;
  @Input() saveLabel = "Save";
  @Input() saveClass = "btn ml-auto btn-primary";

  @Input() trashIcon = faTrashAlt;
  @Input() trashLabel = "";

  spinner = faSpinner;

  static ID = Symbol();

  constructor() {
  }

  ngOnInit() {
  }

  public hasSave(): boolean {
    return this.onSave.observers.length > 0;
  }

  public isSaving() {
    let ret = this.saving;
    while (typeof ret == "function") {
      ret = ret();
    }
    return ret;
  }

  public save(): void {
    this.onSave.emit();
  }

  public hasGoBack(): boolean {
    return this.onGoBack.observers.length > 0;
  }

  public hasProperty(emitter: EventEmitter<any>): boolean {
    return emitter?.observers.length > 0;
  }

  public emitEvent(emitter: EventEmitter<any>): void {
    return emitter?.emit()
  }

  public goBack(): void {
    this.onGoBack.emit();
  }

  public hasDelete(): boolean {
    return this.onDelete.observers.length > 0;
  }

  public delete(): void {
    this.onDelete.emit();
  }
}
