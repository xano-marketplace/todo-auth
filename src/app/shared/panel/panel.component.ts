import {Component, OnDestroy, OnInit} from '@angular/core';
import {PanelService} from "./panel.service";

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export abstract class PanelComponent implements OnInit, OnDestroy {
  public panelSub: any;
  public data: any;
  public open: any;

  protected constructor(
    protected panelService: PanelService,
  ) {
  }

  abstract getComponentId();

  ngOnInit(): void {
    this.panelSub = this.panelService.observe()
      .subscribe(event => {
        if (event.key == this.getComponentId()) {
          if (event.open) {
            this.data = event.data;
            this.onOpen();
          } else {
            this.onClose();
          }
          this.open = event.open;
        }
      });

    this.open = false;
  }

  ngOnDestroy(): void {
    if (this.panelSub) {
      this.panelSub.unsubscribe();
    }
  }

  onOpen(): void {
  }

  onClose(): void {
  }

  close(): void {
    this.panelService.close(this.getComponentId());
  }
}
