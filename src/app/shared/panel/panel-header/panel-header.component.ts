import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faTimes} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-panel-header',
  templateUrl: './panel-header.component.html',
  styleUrls: ['./panel-header.component.scss']
})
export class PanelHeaderComponent implements OnInit {
  public faTimes = faTimes;

  @Input() title;
  @Input() description;
  @Output() onClose = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  public close(): void {
    this.onClose.emit();
  }

}
