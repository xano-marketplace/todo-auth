import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from "../config.service";
import {ConfigPanelComponent} from "../config-panel/config-panel.component";
import {PanelService} from "../shared/panel/panel.service";
import {LoginPanelComponent} from "../login-panel/login-panel.component";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private configService: ConfigService,
    private panelService: PanelService,
    private router: Router,
    private http: HttpClient
  ) {
  }

  public config: XanoConfig;
  private myTemplate: any = "";
  public configured: boolean = false;

  ngOnInit(): void {
    this.config = this.configService.config;
    this.configService.xanoApiUrl.subscribe(apiUrl => this.configured = !!apiUrl)
  }


  openPanel() {
    !this.configured ? ConfigPanelComponent.open(this.panelService, {}) :
      LoginPanelComponent.open(this.panelService, {})
  }

}
