import {Component, EventEmitter, OnInit} from '@angular/core';
import {ConfigService} from "../config.service";
import {Router} from "@angular/router";
import {PanelService} from "../shared/panel/panel.service";
import {ToastService} from "../toast/toast.service";
import {FormGeneratorService} from "../shared/form-generator/form-generator.service";
import {faExclamation, faSearch, faSpinner} from "@fortawesome/free-solid-svg-icons";
import {Validators} from "@angular/forms";
import * as _ from 'lodash';
import {debounceTime, distinctUntilChanged, finalize, tap} from "rxjs/operators";
import {TodoManagePanelComponent} from "./todo-manage-panel/todo-manage-panel.component";
import {TodoService} from "./todo.service";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  public form: any;
  public searchForm: any;
  public success: boolean = false;
  public loading: boolean = false;
  public saving: boolean = false;
  public todos: any[] = [];
  public spinner: any = faSpinner;
  public exclamation: any = faExclamation;
  public search: any = faSearch;
  public currencies: boolean = true;


  constructor(
    private configService: ConfigService,
    private todoService: TodoService,
    private router: Router,
    private toastService: ToastService,
    protected formGeneratorService: FormGeneratorService,
    protected panelService: PanelService,
  ) {
  }

  ngOnInit(): void {
    this.form = this.createForm();

    this.searchForm = this.createSearchForm();

    this.searchForm.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
      )
      .subscribe(() => {
        this.getTodoData()
      });

    this.getTodoData();
  }

  addTodo() {
    let onSuccess = new EventEmitter();
    onSuccess.subscribe(payload => {
      this.toastService.success("Todo added.")
      this.getTodoData();
    });

    TodoManagePanelComponent.open(this.panelService, {
      item: null,
      successEmitter: onSuccess
    });
  }

  editTodo(item) {
    let onSuccess = new EventEmitter;
    onSuccess.subscribe(payload => {
      this.toastService.success("Todo updated.")
      this.getTodoData();
    });

    let onDelete = new EventEmitter;
    onDelete.subscribe(payload => {
      this.toastService.success("Todo deleted.")
      this.getTodoData();
    });

    TodoManagePanelComponent.open(this.panelService, {
      item,
      successEmitter: onSuccess,
      deleteEmitter: onDelete
    });
  }

  updateCompleted(item) {
    this.saveTodo(item);
  }

  saveTodo(item) {
    this.todoService.todoSave(item)
      .pipe(
        tap(x => this.saving = true),
        finalize(() => this.saving = false)
      )
      .subscribe((res: any) => {
        this.toastService.success("Todo updated.");
      }, err => {
        this.toastService.error("An error has occurred.");
      });
  }

  getTodoData() {
    let search = {
      expression: []
    };

    let searchValue = this.searchForm.value.search.trim();

    if (searchValue) {
      search.expression.push({
        statement: {
          left: {
            tag: "col",
            operand: "todo.task",
          },
          op: "ilike",
          right: {
            operand: `%${searchValue}%`,
          }
        }
      });
    }

    if (this.searchForm.value.completed) {
      search.expression.push({
        statement: {
          left: {
            tag: "col",
            operand: "todo.completed",
          },
          right: {
            operand: true,
          }
        }
      });
    }

    if (this.searchForm.value.important) {
      search.expression.push({
        statement: {
          left: {
            tag: "col",
            operand: "todo.important",
          },
          right: {
            operand: true,
          }
        }
      });
    }

    this.todoService.todoGet(search)
      .pipe(
        tap(() => this.loading = true),
        finalize(() => this.loading = false)
      )
      .subscribe((response: any) => {
        this.todos = response;
      }, (err) => {
        this.toastService.error(_.get(err, "error.message", "An unknown error has occurred."));
      });
  }

  private createForm() {
    return this.formGeneratorService.createFormGroup({
      obj: {
        name: ['text', [Validators.required]],
        email: ['email', [Validators.required, Validators.email]],
        company: ['text', []],
        request: ['text', [Validators.required]],
      }
    });
  }

  createSearchForm() {
    return this.formGeneratorService.createFormGroup({
      obj: {
        search: 'text',
        important: 'bool',
        completed: 'bool'
      },
      defaultValue: {
        search: "",
        important: false,
        completed: false
      }
    });
  }


  logout() {
    this.router.navigate(['']).then(x => {
      this.configService.authToken.next(null)
    })
  }

}
