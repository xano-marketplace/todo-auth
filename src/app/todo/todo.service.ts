import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {ApiService} from "../api.service";
import {ConfigService} from "../config.service";

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(private apiService: ApiService, private configService: ConfigService) {
  }

  public todoSave(item): Observable<any> {
    return this.apiService.post({
      endpoint: `${this.configService.xanoApiUrl.value}/todo/${item.id}`,
      headers: {'Authorization': `Bearer ${this.configService.authToken.value}`},
      params: item,
    })
  }

  public todoGet(search): Observable<any> {
    return this.apiService.get({
      endpoint: `${this.configService.xanoApiUrl.value}/todo`,
      headers: {'Authorization': `Bearer ${this.configService.authToken.value}`},
      params: {search}
    })
  }

  public todoDelete(endpoint): Observable<any> {
    return this.apiService.delete({
      endpoint: endpoint,
      headers: {'Authorization': `Bearer ${this.configService.authToken.value}`},
    })
  }

  public todoUpdate(endpoint, value): Observable<any> {
    return this.apiService.post({
      endpoint: endpoint,
      headers: {'Authorization': `Bearer ${this.configService.authToken.value}`},
      params: value
    })
  }
}
