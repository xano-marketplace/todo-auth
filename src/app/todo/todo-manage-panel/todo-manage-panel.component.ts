import {Component, EventEmitter} from '@angular/core';
import {PanelComponent} from "../../shared/panel/panel.component";
import {PanelService} from "../../shared/panel/panel.service";
import {ConfigService} from "../../config.service";
import {FormGeneratorService} from "../../shared/form-generator/form-generator.service";
import {ApiService} from "../../api.service";
import {ToastService} from "../../toast/toast.service";
import {XanoService} from "../../xano.service";
import {Validators} from "@angular/forms";
import {TodoService} from "../todo.service";
import {finalize, tap} from "rxjs/operators";

@Component({
  selector: 'app-todo-manage-panel',
  templateUrl: './todo-manage-panel.component.html',
  styleUrls: ['./todo-manage-panel.component.scss']
})
export class TodoManagePanelComponent extends PanelComponent {
  static ID = Symbol();
  public todoForm;
  public saving: boolean = false;
  public deleting: boolean = false;

  constructor(
    protected panelService: PanelService,
    public configService: ConfigService,
    protected formGeneratorService: FormGeneratorService,
    protected todoService: TodoService,
    protected apiService: ApiService,
    protected toastService: ToastService,
    protected xanoService: XanoService
  ) {
    super(panelService);
  }

  public getTitle(): string {
    return this.data.item ? "Edit Todo" : "Add Todo";
  }

  public getDescription(): string {
    return "Configure your task below";
  }

  public getLabel(): string {
    return this.data.item ? "Update" : "Add";
  }

  public onOpen(): void {
    this.todoForm = this.createForm();
  }

  createForm() {
    let defaultValue = this.data.item || {};

    return this.formGeneratorService.createFormGroup({
      obj: {
        task: ['text', [Validators.required]],
        important: 'bool',
        completed: 'bool',
      },
      defaultValue
    });
  }

  getComponentId() {
    return TodoManagePanelComponent.ID;
  }

  static open(panel: PanelService, args: { item: any, successEmitter?: EventEmitter<any>, deleteEmitter?: EventEmitter<any> }) {
    panel.open(this.ID, 'Todo', {
      ...args
    });
  }

  public getEndpoint(): string {
    return this.data.item ? `${this.configService.xanoApiUrl.value}/todo/${this.data.item.id}` : `${this.configService.xanoApiUrl.value}/todo`
  }

  delete() {
    this.todoService.todoDelete(this.getEndpoint())
      .pipe(
        tap(() => this.saving = true),
        finalize(() => this.saving = false)
      )
      .subscribe((res: any) => {
        if (this.data.deleteEmitter) {
          this.data.deleteEmitter.emit(res)
        }
        this.close();
      }, err => {
        this.toastService.error("An error has occurred.");
      });
  }

  save() {
    if (!this.todoForm.trySubmit()) return;

    this.todoService.todoUpdate(this.getEndpoint(), this.todoForm.value)
      .pipe(
        tap(() => this.saving = true),
        finalize(() => this.saving = false)
      )
      .subscribe((res: any) => {
        if (this.data.successEmitter) {
          this.data.successEmitter.emit(res)
        }
        this.close();
      }, err => {
        this.toastService.error("An error has occurred.");
      });
  }
}
