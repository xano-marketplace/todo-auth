import {Component} from '@angular/core';
import {PanelService} from "../shared/panel/panel.service";
import {ConfigService} from "../config.service";
import {FormGeneratorService} from "../shared/form-generator/form-generator.service";
import {ApiService} from "../api.service";
import {XanoService} from "../xano.service";
import {Validators} from "@angular/forms";
import {PanelComponent} from "../shared/panel/panel.component";
import {ToastService} from "../toast/toast.service";

@Component({
  selector: 'app-config-panel',
  templateUrl: './config-panel.component.html',
  styleUrls: ['./config-panel.component.scss']
})
export class ConfigPanelComponent extends PanelComponent {

  static ID = Symbol();
  public setupForm: any;
  public saving: any;

  constructor(
    protected panelService: PanelService,
    public configService: ConfigService,
    protected formGeneratorService: FormGeneratorService,
    protected apiService: ApiService,
    protected toastService: ToastService,
    protected xanoService: XanoService
  ) {
    super(panelService);
  }

  onOpen() {
    this.setupForm = this.createForm();
  }

  createForm() {
    return this.formGeneratorService.createFormGroup({
      obj: {
        xanoApiUrl: ['text', [Validators.required, Validators.pattern('^https?://.+')]],
      },
      defaultValue: {
        xanoApiUrl: this.configService.xanoApiUrl.value
        // ...this.configService
      }
    });
  }

  getComponentId() {
    return ConfigPanelComponent.ID;
  }

  static open(panel: PanelService, args: {}) {
    panel.open(this.ID, 'Setup', {...args});
  }

  save() {
    if (!this.setupForm.trySubmit()) return;

    this.apiService.get({
      endpoint: this.xanoService.getApiSpecUrl(this.setupForm.value.xanoApiUrl),
      headers: {
        Accept: 'text/yaml'
      },
      responseType: "text",
      stateFunc: state => this.saving = state
    }).subscribe((res: any) => {
      if (!this.configService.config.requiredApiPaths.every(path => res.includes(path))) {
        this.toastService.error("This Xano Base URL is missing the required endpoints. Have you installed this marketplace extension?");
        return;
      }
      this.configService.xanoApiUrl.next(this.xanoService.getApiUrl(this.setupForm.value.xanoApiUrl))
      this.close();
    }, err => {
      this.toastService.error("An error has occurred.");
    });
  }
}
