import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";

export interface XanoConfig {
  title: string,
  summary: string,
  editText: string,
  editLink: string,
  descriptionHtml: string,
  logoHtml: string,
  requiredApiPaths: string[]
}

@Injectable({
  providedIn: 'root'
})

export class ConfigService {
  public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>(null)
  public authToken: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  public config: XanoConfig = {
    title: 'Authenticated Todo List',
    summary: 'This extension provides a fully functioning demo to manage a private personal todo list.',
    editText: 'Get source code',
    editLink: '',
    descriptionHtml: `
                <h4>Description</h4>
                <p>This demo consists of following components:</p>
                <h5>Personal View</h5>
                <p>This is your authenticated todo list view where you see a list of private tasks. Each task has some functionality to edit the task, mark it important, or complete it.</p>
                <h5>Manage View</h5>
                <p>This is a side panel that lets you add a new todo task or edit an existing one. The edit component also supports deleting a task.</p>
                <h5>Search</h5>
                <p>A search box is present to allow you to search content from with each task description. There is also support for filtering based on completed and important attributes.</p>
                `,
    logoHtml: '',
    requiredApiPaths: [
      '/auth/login',
      '/todo',
      '/todo/{todo_id}'
    ]
  };

  constructor() {
  }

  public isConfigured(): Observable<any> {
    return this.xanoApiUrl.asObservable();
  }


}
