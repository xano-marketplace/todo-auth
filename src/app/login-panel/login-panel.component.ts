import {Component} from '@angular/core';
import {PanelService} from "../shared/panel/panel.service";
import {ConfigService} from "../config.service";
import {FormGeneratorService} from "../shared/form-generator/form-generator.service";
import {XanoService} from "../xano.service";
import {Validators} from "@angular/forms";
import {PanelComponent} from "../shared/panel/panel.component";
import {LoginService} from "./login.service";
import {ToastService} from "../toast/toast.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login-panel',
  templateUrl: './login-panel.component.html',
  styleUrls: ['./login-panel.component.scss']
})
export class LoginPanelComponent extends PanelComponent {

  static ID = Symbol();
  public loginForm: any;
  public saving: any;
  private emailRegex = new RegExp('^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$');

  constructor(
    protected panelService: PanelService,
    public configService: ConfigService,
    protected formGeneratorService: FormGeneratorService,
    private loginService: LoginService,
    private router: Router,
    protected toastService: ToastService,
    protected xanoService: XanoService
  ) {
    super(panelService);
  }

  onOpen() {
    this.loginForm = this.createForm();
  }

  createForm() {
    return this.formGeneratorService.createFormGroup({
      obj: {
        email: ['email', [Validators.required, Validators.pattern(this.emailRegex)]],
        password: ['password', [Validators.required]]
      },
    });
  }

  getComponentId() {
    return LoginPanelComponent.ID;
  }

  static open(panel: PanelService, args: {}) {
    panel.open(this.ID, 'Setup', {...args});
  }

  save() {
    if (!this.loginForm.trySubmit()) return;

    this.loginService.login(this.loginForm.getRawValue()).subscribe(authToken => {
      if (authToken) {
        this.configService.authToken.next(authToken.authToken);
        this.router.navigate(['/todo']).then(x => this.close())

      } else {
        this.toastService.error('No auth token')
      }
    }, err => {
      this.toastService.error(err.error.message);
    })
  }

}
