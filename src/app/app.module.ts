import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRouter} from './app.router';
import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HomeComponent} from './home/home.component';
import {TodoComponent} from './todo/todo.component';
import {SharedModule} from "./shared/shared.module";
import {TodoManagePanelComponent} from './todo/todo-manage-panel/todo-manage-panel.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ConfigPanelComponent} from './config-panel/config-panel.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {LoginPanelComponent} from './login-panel/login-panel.component';
import {ToastComponent} from './toast/toast.component';
import {LocationStrategy, PathLocationStrategy} from "@angular/common";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TodoComponent,
    TodoManagePanelComponent,
    ConfigPanelComponent,
    LoginPanelComponent,
    ToastComponent
  ],
  imports: [
    BrowserModule,
    AppRouter,
    HttpClientModule,
    SharedModule,
    NgbModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [{provide: LocationStrategy, useClass: PathLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
